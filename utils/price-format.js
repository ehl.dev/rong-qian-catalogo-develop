export const priceFormat = (value) => {
    // https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat

    const formatter = new Intl.NumberFormat('es-PE', {
        style: 'currency',
        currency: 'PEN',
        useGrouping: true
    })

    let result = formatter.format(value)

    return result

}