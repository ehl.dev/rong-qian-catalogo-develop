export const state = () => ({
    recentSearches: []
})

export const getters = {
    getRecentSearches: state => {
        let items = JSON.parse(JSON.stringify(state.recentSearches))
        return items.reverse()
    }
}

export const mutations = {
    SET_RECENT_SEARCHES(state, items) {
        state.recentSearches = items
    }
}