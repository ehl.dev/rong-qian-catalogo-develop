export const state = () => ({
    user: null,
    loading: true
})

export const mutations = {
    SET_LOADER(state, value) {
        state.loading = value
    }
}

export const getters = {
    isLoading(state) {
        return state.loading
    }
}