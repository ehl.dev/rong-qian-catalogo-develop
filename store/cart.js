export const state = () => ({
    cart: []
})

export const mutations = {
    SET_PRODUCTS(state, items) {
        state.cart = items

        localStorage.setItem('rong_qian_cart', JSON.stringify(items))
    },
    ADD_PRODUCT(state, data) {
        let product = data.product
        let cart = state.cart

        let existInCart = cart.find(i => i.id == product.id)

        if (existInCart) {
            let index = cart.findIndex(i => i.id == product.id)
            cart[index].quantity += data.quantity
        } else {
            cart.push({
                id: product.id,
                slug: product.slug,
                quantity: data.quantity
            })
        }

        this.commit("cart/SET_PRODUCTS", cart)
    },
    REMOVE_PRODUCT(state, index) {
        let cart = [...state.cart]

        cart.splice(index, 1)

        this.commit("cart/SET_PRODUCTS", cart)
    },
    ADD_QUANTITY(state, index) {
        let cart = [...state.cart]

        cart[index].quantity += 1

        this.commit("cart/SET_PRODUCTS", cart)
    },
    DIMINISH_QUANTITY(state, index) {
        let cart = [...state.cart]

        cart[index].quantity -= 1

        this.commit("cart/SET_PRODUCTS", cart)
    }
}

export const getters = {
    getCart(state) {
        return state.cart
    },
    getTotalProducts(state) {
        return state.cart.length
    },
    getSubTotal(state) {
        let total = 0
        
        state.cart.forEach(item => {
            let totalByProduct = item.quantity * (item.precio_descuento ? item.precio_descuento : item.precio_real)

            total += totalByProduct
        })

        return total
    }
}