export const getRecentSearches = () => {
    let recentSearches = localStorage.getItem('recent-searches')
    recentSearches = JSON.parse(recentSearches)

    if(!recentSearches) {
        localStorage.setItem('recent-searches', JSON.stringify([]))
    }

    return recentSearches
}

export const addRecentSearches = (value) => {
    let recentSearches = localStorage.getItem('recent-searches')
    recentSearches = JSON.parse(recentSearches)

    recentSearches.push(value)

    localStorage.setItem('recent-searches', JSON.stringify(recentSearches))
}

export const removeRecentSearches = (value) => {
    let recentSearches = localStorage.getItem('recent-searches')
    recentSearches = JSON.parse(recentSearches)

    let indexOfItem = recentSearches.indexOf(value)

    recentSearches.splice(indexOfItem, 1)

    localStorage.setItem('recent-searches', JSON.stringify(recentSearches))

    return recentSearches
}