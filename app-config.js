export const appConfig = {
    name: 'Rong Qian',
    logo: 'https://res.cloudinary.com/ehldev/image/upload/v1654279562/logo_qicugd.svg',
    graphql_endpoint: 'https://api.rongqian.pe/public/graphql',
    pagination: {
        number_paginate: 16
    },
    contact: {
        whatsapp: '+51943336009',
        maps: 'https://www.google.com/maps/place/Jr.+Puno+828,+Cercado+de+Lima+15001/@-12.0549159,-77.0252657,17z/data=!3m1!4b1!4m5!3m4!1s0x9105c8bbaeed1edd:0x17d67494531416eb!8m2!3d-12.0549159!4d-77.0252657',
        waze: 'https://www.waze.com/es-419/live-map/directions/pe/provincia-de-lima/cercado-de-lima/jr.-puno-828?to=place.ChIJ3R7trrvIBZER6xYUU5R01hc',
        facebook: 'https://www.facebook.com/RongQian828/',
        tiktok: 'https://www.tiktok.com/@import_rongqian_aoyue?_d=secCgYIASAHKAESPgo8s2W4I6MEIz21SOmap5vItBVIi%2FEemYexQGAxWzobP9lG69AP54xK2nxynt6R2T2nUfxMgcTJC6G0ey3WGgA%3D&_r=1&checksum=3da7319f9b735f8ab64ef47cda1e75fa30c3c64e417557d82e7d565b23c7fc57&language=es&sec_uid=MS4wLjABAAAA08aRLWW0hUFzU4ghfedx2_fW6ziiuFPZX14Yw3pvrQMDN722x1PXoU8BQJY7giTf&sec_user_id=MS4wLjABAAAAdnYVYJY9n3-nugzjTJqFztW7p0XRMHi8D2qUws3zy9LhnuQFRC9ve40t5l76hxmv&share_app_id=1233&share_author_id=6860920919196746758&share_link_id=E8296454-F697-4171-A6C1-B732450F5930&source=h5_m&tt_from=copy&u_code=dbbkcl2kk5l95j&ug_btm=b2878%2Cb5836&user_id=6804866279751009285&utm_campaign=client_share&utm_medium=ios&utm_source=copy'
    },
    openGraph: {
        titulo: 'Rong Qian',
        descripcion: 'Tienda con variedad de productos',
        logo: 'https://res.cloudinary.com/ehldev/image/upload/v1654279562/logo_qicugd.svg',
        logoWhatsapp: 'https://res.cloudinary.com/ehldev/image/upload/v1654279562/logo_qicugd.svg',
        urlWeb: 'https://rongqian.pe/',
        keywords: 'Tienda variada'
    }
}
