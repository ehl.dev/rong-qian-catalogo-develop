let cart = [
    {
        id: 1,
        name: 'ATUN TROZOS FLORIDA 170GR + VINAGRE TINTO FLORIDA 125ML',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/florida.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/florida.webp',
        realPrice: 10.00,
        offerPrice: 5.00
    },
    {
        id: 2,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Fritas',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        realPrice: 20.00,
        offerPrice: null
    },
    {
        id: 2,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Fritas',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        realPrice: 20.00,
        offerPrice: null
    },
    {
        id: 2,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Fritas',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        realPrice: 20.00,
        offerPrice: null
    }
]

function string_to_slug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

cart.forEach(item => {
    item.slug = string_to_slug(item.name)
})

export default cart