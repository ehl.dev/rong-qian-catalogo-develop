let products = [
    {
        id: 1,
        name: 'ATUN TROZOS FLORIDA 170GR + VINAGRE TINTO FLORIDA 125ML',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/florida.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/florida.webp',
        realPrice: 10.00,
        offerPrice: 5.00,
        galeria: [
            {
                id: 1,
                url: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagenes-tienda-copy-334-copy-.webp'
            },
            {
                id: 2,
                url: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagenes-tienda-copy-345-copy-.webp'
            },
            {
                id: 3,
                url: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagenes-tienda-copy-334-copy-.webp'
            },
            {
                id: 4,
                url: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagenes-tienda-copy-345-copy-.webp'
            },
            {
                id: 5,
                url: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagenes-tienda-copy-334-copy-.webp'
            },
            {
                id: 6,
                url: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagenes-tienda-copy-345-copy-.webp'
            }
        ],
        category: {
            id: 1,
            name: 'Abarrotes'
        }
    },
    {
        id: 2,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Fritas',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        realPrice: 20.00,
        offerPrice: null
    },
    {
        id: 3,
        name: 'Aceite Vegetal del cielo 1LT',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-01/cielo.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-01/cielo.webp',
        realPrice: 100.00,
        offerPrice: 25.00
    },
    {
        id: 4,
        name: 'Goma de Mascar TRIDENT Fruit Mix Uva y Limonada Paquete 30.6g',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagenes-tienda-copy-357-copy-.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagenes-tienda-copy-357-copy-.webp',
        realPrice: 10.00,
        offerPrice: null
    },
    {
        id: 5,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Frit...',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        realPrice: 10.00,
        offerPrice: 5.00
    },
    {
        id: 6,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Frit...',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        realPrice: 795.00,
        offerPrice: null
    },
    {
        id: 7,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Frit...',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        realPrice: 10.00,
        offerPrice: 5.00
    },
    {
        id: 8,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Frit...',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagen-tienda-13.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2020-11/imagen-tienda-13.webp',
        realPrice: 10.00,
        offerPrice: 5.00
    },
    {
        id: 9,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Frit...',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        realPrice: 10.00,
        offerPrice: 5.00
    },
    {
        id: 10,
        name: 'Cerveza Cusqueña Negra Pack 6 Botellas de 330 ml c/u + Papas Frit...',
        shortDescription: 'Producto para el hogar',
        largeDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, iure esse officia eligendi laboriosam, libero inventore illum possimus necessitatibus recusandae delectus voluptas eum nemo accusantium alias ea tenetur? Labore, quibusdam!',
        mainImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        secondaryImage: 'https://logica.alimarket.pe/storage/app/imagenesGenerales/2021-02/cuzq3.webp',
        realPrice: 10.00,
        offerPrice: 5.00
    }
]

function string_to_slug(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to = "aaaaeeeeiiiioooouuuunc------";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

products.forEach(item => {
    item.slug = string_to_slug(item.name)
})

export default products