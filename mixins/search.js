import { mapGetters } from 'vuex'
import { getRecentSearches, addRecentSearches } from '@/services/recent-searches.js'

import SearchBadge from '../components/global/badges/SearchBadge.vue'

export default {
    data() {
        return {
            query: null,
            editing: false
        }
    },
    mounted() {
        if (process.client) this.initRecentSearches()
    },
    components: {
        SearchBadge
    },
    methods: {
        initRecentSearches() {
            this.query = null
            this.editing = false

            const recentSearches = getRecentSearches()
            this.$store.commit('search/SET_RECENT_SEARCHES', recentSearches)
        },
        search() {
            addRecentSearches(this.query)

            this.$router.push({ name: 'buscar-productos', query: { q: this.query } })

            this.$bvModal.hide('modal-mobile-search')

            this.initRecentSearches()
        }
    },
    computed: {
        ...mapGetters({
            getRecentSearches: 'search/getRecentSearches'
        })
    }
}