import GetCategorias from "@/graphql/queries/productos/categorias/GetCategorias.gql"

export default {
    data() {
        return {
            categorias: []
        }
    },
    created() {
        this.getCategories()
    },
    methods: {
        async getCategories() {
            let estado = "Activado";

            let response = await this.$apollo
                .query({
                    query: GetCategorias,
                    fetchPolicy: "no-cache",
                    variables: {
                        estado,
                    }
                })

            this.categorias = response.data.GetCategoria
        }
    }
}