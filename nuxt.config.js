import { appConfig } from "./app-config";
import axios from "axios"

export default {
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  // Global page headers: https://go.nuxtjs.dev/config-head
  mode: 'universal',
  server: { port: 3000, host: 'localhost' },
  target: 'server',
  head: {
    title: appConfig.name,
    htmlAttrs: {
      lang: 'es'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/hamburgers/1.2.1/hamburgers.min.css' }
    ],
    script: [
      {
        src: 'https://kit.fontawesome.com/3bd84f9f96.js'
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/index.scss',
    'vue-slick-carousel/dist/vue-slick-carousel.css',
    'animate.css/animate.min.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/vue-slick-carousel.js' },
    { src: '@/plugins/vuelidate.js' },
    { src: '@/plugins/smooth-scroll.js', mode: "client" },
    { src: '@/plugins/vue-easy-lightbox.js', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/apollo',
    '@nuxtjs/toast',
    '@nuxtjs/sitemap',
    '@nuxtjs/pwa'
  ],
  bootstrapVue: {
    bootstrapCSS: false, // here you can disable automatic bootstrapCSS in case you are loading it yourself using sass
    bootstrapVueCSS: false, // CSS that is specific to bootstrapVue components can also be disabled. That way you won't load css for modules that you don't use
    componentPlugins: ['Modal', 'Button', 'FormCheckbox', 'Collapse', 'FormRadio', 'Card', 'Pagination', 'Carousel']
  },
  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: appConfig.graphql_endpoint,
        GRAPHQL_DEBUG: true
      }
    }
  },
  sitemap: {
    hostname: 'https://rongqian.pe',
    routes: async () => {
      // Rutas blog
      let response = await axios.post(appConfig.graphql_endpoint, {
        query: `
        query GetProductos($page:Int,$number_paginate:Int,$estado:String){
          GetProductos(page:$page,number_paginate:$number_paginate,estado:$estado) {
            NroItems
            data {
              id
              nombre
              slug
              descripcionCorta
            }
          }
        }
          `,
        variables: {
          page: 1,
          number_paginate: 100,
          estado: ""
        }
      });

      let routesBlog = []
      response.data.data.GetProductos.data.forEach(item => routesBlog.push(`/productos/${item.slug}`))

      return routesBlog
    },
    cacheTime: 1000 * 60 * 60 * 2,
    trailingSlash: true,
    gzip: true,
    defaults: {
      changefreq: "daily",
      priority: 1,
      lastmod: new Date()
    },
    pwa: {
      meta: {
        title: 'Rong Qian',
        author: "Softaki"
      },
      manifest: {
        name: 'Rong Qian',
        short_name: 'Rong Qian',
        lang: 'es'
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
