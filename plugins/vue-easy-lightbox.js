// global registration in the entry file, e.g. main.js
import Vue from 'vue'
import VueEasyLightbox from 'vue-easy-lightbox'

Vue.use(VueEasyLightbox)