// https://github.com/gs-shop/vue-slick-carousel/blob/master/docs/API.md#props

import Vue from 'vue'

import VueSlickCarousel from 'vue-slick-carousel'

Vue.component('VueSlickCarousel', VueSlickCarousel)